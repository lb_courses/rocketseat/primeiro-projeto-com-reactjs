/* eslint-disable react/static-property-placement */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import { FaLongArrowAltLeft, FaLongArrowAltRight } from 'react-icons/fa';
import api from '../../services/api';
import { Loading, Owner, IssueList, NavPages } from './styles';
import Container from '../../components/Container/index';

export default class Reposity extends Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        repository: PropTypes.string,
      }),
    }).isRequired,
  };

  state = {
    repository: {},
    issues: [],
    loading: true,
    page: 1,
    filtro: '',
  };

  async componentWillMount() {
    await this.getApi();
  }

  async friltro(event = 'all') {
    this.setState({ page: 1 });
    await this.setState({ filtro: event.value });
    await this.getApi();
  }

  async paginacao(acao) {
    const { page } = this.state;
    if (acao === '-') {
      if (page <= 1) return false;
      await this.setState({ page: page - 1 });
      await this.getApi(page - 1);
    } else {
      await this.setState({ page: page + 1 });
      await this.getApi(page + 1);
    }
  }

  async getApi(page = 1) {
    const { match } = this.props;

    const repoName = decodeURIComponent(match.params.repository);
    const { filtro } = this.state;
    console.log('filtro => ', filtro);
    const [repository, issues] = await Promise.all([
      api.get(`/repos/${repoName}`),
      api.get(`/repos/${repoName}/issues`, {
        params: {
          state: filtro || 'all',
          page,
          per_page: 5,
        },
      }),
    ]);

    const i = issues.data;

    this.setState({
      repository: repository.data,
      issues: i,
      loading: false,
    });

    console.log(this.state);
  }

  render() {
    const options = [
      { value: 'all', label: 'All' },
      { value: 'open', label: 'Open' },
      { value: 'closed', label: 'Closed' },
    ];

    const { repository, issues, loading, page } = this.state;

    // console.log(issues);
    if (loading) {
      return <Loading>Carregando</Loading>;
    }

    return (
      <Container>
        <Owner>
          <Link to="/">Voltar aos repositórios</Link>
          <img src={repository.owner.avatar_url} alt={repository.owner.login} />
          <h1>{repository.name}</h1>
          <p>{repository.description}</p>
          <Select
            className="widthSelect"
            options={options}
            onChange={event => this.friltro(event)}
          />
        </Owner>

        <NavPages>
          <div>
            <FaLongArrowAltLeft onClick={() => this.paginacao('-')} />
          </div>
          <h3>Page: {page}</h3>
          <div>
            <FaLongArrowAltRight onClick={() => this.paginacao('+')} />
          </div>
        </NavPages>
        <IssueList>
          {issues.map(issue => (
            <li key={String(issue.id)}>
              <img src={issue.user.avatar_url} alt={issue.user.login} />
              <div>
                <strong>
                  <a href={issue.html_url}>{issue.title}</a>
                  {issue.labels.map(label => (
                    <span key={String(label.id)}>{label.name}</span>
                  ))}
                </strong>
                <p>{issue.user.login}</p>
              </div>
            </li>
          ))}
        </IssueList>
        {/* <button onClick={() => this.paginacao()}>Mais</button> */}
      </Container>
    );
  }
}
