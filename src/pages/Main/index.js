import React, { Component } from 'react';
import { FaGithubAlt, FaPlus, FaSpinner } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { Form, SubmitButton, List } from './styles';
import Container from '../../components/Container/index';

import api from '../../services/api';

export default class Main extends Component {
  state = {
    newRepo: '',
    repositories: [],
    loading: false,
    error: true,
  };

  // carrega os dados do localStorage
  componentDidMount() {
    const repositores = localStorage.getItem('repositores');
    if (repositores) {
      this.setState({ repositories: JSON.parse(repositores) });
    }
  }

  // salva os dados do localStorage
  componentDidUpdate(_, prevState) {
    const { repositories } = this.state;

    if (prevState !== repositories) {
      localStorage.setItem('repositores', JSON.stringify(repositories));
    }
  }

  handleInputChange = e => {
    this.setState({ newRepo: e.target.value });
  };

  // eslint-disable-next-line consistent-return
  handleSubmit = async e => {
    e.preventDefault();

    this.setState({ loading: true });
    const { newRepo, repositories } = this.state;

    const existRepo = repositories.find(repo => repo.name === newRepo);

    if (existRepo) {
      alert('Repositório duplicado!');
      this.setState({ loading: false, error: false });
      // throw new Error('Repositório duplicado');
      return true;
    }

    try {
      const response = await api.get(`/repos/${newRepo}`);
      const data = {
        name: response.data.full_name,
      };
      this.setState({
        repositories: [...repositories, data],
        newRepo: '',
        loading: false,
        error: true,
      });
    } catch (error) {
      this.setState({ loading: false, error: false });
    }
  };

  render() {
    const { newRepo, repositories, loading, error } = this.state;
    return (
      // <Title error={false}>
      //   Main
      //   <small>menor</small>
      // </Title>

      <Container>
        <h1>
          <FaGithubAlt />
          Repositórios
        </h1>

        <Form error={error} onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="Adicionar repositório"
            value={newRepo}
            onChange={this.handleInputChange}
          />

          <SubmitButton loading={loading ? 1 : 0}>
            {loading ? (
              <FaSpinner color="#FFF" size={14} />
            ) : (
              <FaPlus color="#FFF" size={14} />
            )}
          </SubmitButton>
        </Form>

        <List>
          {repositories.map(repository => (
            <li key={repository.name}>
              <span>{repository.name}</span>
              <Link to={`/repository/${encodeURIComponent(repository.name)}`}>
                Detalhes
              </Link>
            </li>
          ))}
        </List>
      </Container>
    );
  }
}
