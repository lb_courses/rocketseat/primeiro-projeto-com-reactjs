import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// yarn add eslint -D
// yarn eslint --init
// yarn add prettier eslint-config-prettier eslint-plugin-prettier babel-eslint  -D
// yarn add react-router-dom
// yarn add styled-components
// yarn add react-icons
// yarn add axios
// yarn prop-types
ReactDOM.render(<App />, document.getElementById('root'));
